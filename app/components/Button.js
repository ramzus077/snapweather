import React, {useEffect} from 'react';
import {GET_CITY_WEATHER_REQUEST} from '../models/city/actions';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

// const mapDispatchToProps = (dispatch, props) => ({
//   getCityWeather: () => {
//     dispatch({
//       type: GET_CITY_WEATHER_REQUEST,
//       payload: {},
//     });
//   },
// });

const Button = ({label, onPress}) => {
  // useEffect(() => {
  //   getCityWeather();
  // }, [getCityWeather]);

  const {buttonStyle, labelStyle} = styles;
  return (
    <TouchableOpacity style={buttonStyle} onPress={onPress}>
      <Text style={labelStyle}>{label}</Text>
    </TouchableOpacity>
  );
};
//const Button = connect(mapStateToProps, mapDispatchToProps)(HomeView);

export default Button;

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: '#ffd000',
    justifyContent: 'center',
    height: 48,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
  },
  labelStyle: {
    textAlign: 'center',
    fontSize: 18,
  },
});
