import React, {useEffect} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {GET_CITY_WEATHER_REQUEST} from '../models/city/actions';
import Button from '../components/Button';
const mapStateToProps = (state, props) => {
  const {id, city, degree, description} = state.city;

  return {id, city, degree, description};
};

const mapDispatchToProps = (dispatch, props) => ({
  getCityWeather: () => {
    dispatch({
      type: GET_CITY_WEATHER_REQUEST,
      payload: {},
    });
  },
});

const Cities = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    city: 'Montreal',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    city: 'New York',
  },
  {id: '58694a0f-3da1-471f-bd96-145571e29d72', city: 'Miami'},
];

const HomeView = ({navigation}) => {
  // useEffect(() => {
  //   getCityWeather();
  // }, [getCityWeather]);

  const renderItem = ({item}) => <Button label={item.city} />;

  return (
    <View style={styles.containerStyle}>
      <FlatList
        data={Cities}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

const Home = connect(mapStateToProps, mapDispatchToProps)(HomeView);

export {Home};

const styles = StyleSheet.create({
  containerStyle: {
    marginTop: 30,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
  },
});
