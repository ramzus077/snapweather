import {GET_CITY_WEATHER_SUCCESS} from './actions';

const initialState = {
  id: '1',
  city: 'Montreal',
  degree: '0',
  description: 'snow',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CITY_WEATHER_SUCCESS: {
      const {id, city, degree, description} = action.payload;

      return {
        id,
        city,
        degree,
        description,
      };
    }
    default:
      return state;
  }
};

export {reducer};
