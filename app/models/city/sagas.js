import {takeEvery, put} from 'redux-saga/effects';
import {GET_CITY_WEATHER_REQUEST, GET_CITY_WEATHER_SUCCESS} from './actions';

function* handler() {
  yield takeEvery(GET_CITY_WEATHER_REQUEST, getCityWeather);
}

function* getCityWeather(action) {
  try {
    // API call
    yield put({
      type: GET_CITY_WEATHER_SUCCESS,
      // TODO: SET THE DATA FROM API
      payload: {
        id: '1',
        city: 'Montreal',
        degree: 0,
        description: 'snow',
      },
    });
  } catch (err) {
    // Handle error
  }
}

export {handler};
