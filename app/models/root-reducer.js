import {combineReducers} from 'redux';
import {reducer as userReducer} from './city/reducers';

const reducer = combineReducers({
  city: userReducer,
});

export {reducer};