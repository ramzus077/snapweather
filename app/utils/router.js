import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import {Home} from '../home';
import {SNAP_WEATHER} from '../utils/constants';

const MainStack = createStackNavigator({
  'Snap Weather': {
    screen: Home,
  },
});

const Navigation = createAppContainer(MainStack);
export {Navigation};